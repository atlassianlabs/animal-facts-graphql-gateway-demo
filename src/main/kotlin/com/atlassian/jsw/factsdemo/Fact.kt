package com.atlassian.jsw.factsdemo

data class Fact(val fact: String, val length: Int, val latency: Long?)
